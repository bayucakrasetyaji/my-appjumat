import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './history';

import Drawer from "./views/Drawer"
import Header from "./views/Header"

import HomePage from './views/HomePage';
import MyTimeline from './views/MyTimeline';

function AppRouter() {
    return(
        <>
            <Drawer />
            <Router history={history}>
                <Header />
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/my-tl" component={MyTimeline} />
                </Switch>
            </Router>
        </>
    );
}
export default AppRouter;