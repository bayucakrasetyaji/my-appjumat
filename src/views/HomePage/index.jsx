import React, { Component } from 'react';
import { Container, CssBaseline, Grid, Paper } from '@material-ui/core';
import './HomePageStyle.css';
import Vector from '../../assets/image/vector1.svg'


class HomePage extends Component {
    render() {
        return (
            <>
            <div className="Content" maxWidth="lg">
                <div className="Content__Right">
                    <Grid className="GridContainer" container direction="row" justify="space-between" alignItems="center">
                        <Grid className="Box-Text" item lg={6}> 
                            <span className="Text">
                            Hello
                            </span>
                            <br/>
                            <span className="Text2">
                            I'm
                            <span className="text2-1">
                            {" "} Bayu
                            </span>
                            </span>
                            <br/>
                            <span className="Text3">
                            Cakra
                            </span>
                        </Grid>
                    <Grid item lg={6}>
                        <img className="GambarVector"  src={Vector} alt="dummy"/> 
                    </Grid>
                </Grid>
                </div>
            </div>
            
            </>
        )
    }

}

export default HomePage;
