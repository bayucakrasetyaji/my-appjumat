import React, { Component } from 'react';
import './MyTimelineStyle.css';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme)=>({
    table: {
      minWidth: 600,
    },
    pageroot: {
        '& > *': {
          marginTop: theme.spacing(2),
        },
      },
  }));

  function createData(no, fitur, targetselesai, status) {
    return { no, fitur, targetselesai, status};
  }

  function PaginationRounded() {
    const classes = useStyles();
  
    return (
      <div className={classes.pageroot}>
        <Pagination count={10} shape="rounded" />
      </div>
    );
  }
const rows = [
    createData(1, 'Install Dependency dan Asset React', '05-03-2021', 'ON PROGRESS'),
    createData(2, 'Mempelajari Component React JS', '08-03-2021', 'ON PROGRESS'),
    createData(3, 'Mempelajari Modul Dependency Frontend', '10-03-2021','ON PROGRESS'),
    createData(4, 'Membuat Frontend Menggunakan Component','12-03-2021', 'ON PROGRESS'),
    createData(5, 'Membuat Frontend Form Login', '16-03-2021', 'ON PROGRESS'),
  ];

export default function MyTimeline () {

    const classes = useStyles();
        return (
            <>
            <div>
            <div className="Text-Content">
                <h1 className="Tittle">My Timeline</h1>
                <h2 className="SubTittle">Timeline pembelajaran sebagai front-end <br/>
                developer dalam sebulan</h2>
            </div>
            </div>
            <TableContainer classes={{root: 'TableContainer'}} component={Paper}>  {/*kalau classes biasanya pake root di kasih classes, lalu di style dikasih !important*/}
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell className="Header_No">No</TableCell> {/*beri classname untuk mengedit style di css*/}
            <TableCell align="left">Fitur Utama</TableCell>
            <TableCell align="left">Tanggal</TableCell>  {/*&nbsp; adalah spasi*/}
            <TableCell align="left">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.no}>
              <TableCell component="th" scope="row">
                {row.no}
              </TableCell>
              <TableCell align="left">{row.fitur}</TableCell>
              <TableCell align="left">{row.targetselesai}</TableCell>
              <TableCell align="left">{row.status}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    <PaginationRounded />
            </>
        );
    }



