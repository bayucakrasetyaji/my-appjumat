import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import './HeaderStyle.css';


export default class Header extends Component {
    render() {
        return (
            <div className="Header">
            <div className="Menu">
                    <NavLink className="Home" to="/">
                        Home
                    </NavLink>
                    <NavLink className="My-Timeline" to="/my-tl">
                    My Timeline
                    </NavLink>
                    </div>
            </div>
        )
    }
}
