import './App.css';
// import Welcome from "./Component"
// import LoginPage from './views/LoginPage'
import AppRouter from "./router";


function App() {
  return (
    <div className="App">
      <AppRouter/>
    </div>
  );
}

export default App;
